package com.example.RestaurantProject.service;

import com.example.RestaurantProject.entity.Customer;
import com.example.RestaurantProject.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    CustomerRepository customerRepository;
    @Override
    public String addCustomer(Customer customer) {
         customerRepository.save(customer);
         return "ADDED CUSTOMER";
    }

    @Override
    public Customer viewCustomerById(Long customerId) {
         return customerRepository.findById(customerId).get();
    }
}
