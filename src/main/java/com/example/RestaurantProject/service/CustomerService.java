package com.example.RestaurantProject.service;

import com.example.RestaurantProject.entity.Customer;

import java.util.Optional;

public interface CustomerService {
    public String addCustomer(Customer customer);
    public  Customer  viewCustomerById(Long customerId);
}
