package com.example.RestaurantProject.controller;

import com.example.RestaurantProject.entity.Customer;
import com.example.RestaurantProject.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @PostMapping()
    public String addCustomer(@RequestBody Customer customer){
        return customerService.addCustomer(customer);
    }

    @GetMapping("/get-by-id/{customerId}")
    public  Customer getCustomer(@PathVariable("id") int id){

        return customerService.viewCustomerById((long) id);
    }

}
